provider "aws" {
  region = "us-east-1"
  access_key = file("../access_key.txt")
  secret_key = file("../secret_key.txt")
}

resource "aws_instance" "web" {
  ami           = "ami-0652d6c7a2e2d090a"
  instance_type = "t2.micro"
}

